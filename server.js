const express = require('express');
const bodyParser = require('body-parser').json();

/* Elastic Search */
var elasticSearch = require('elasticsearch');
var clientElastic = elasticSearch.Client({
    host: 'http://172.18.10.135:9200'
});

/* Serveur */ 
const app = express();
let PORT_NUMBER = 8080;

/* RabbitMQ */
var amqp = require('amqplib/callback_api');
var pubChannel; 
const CLOUDAMQP_URL = 'amqp://admin:password@172.18.10.134:5672'; 

amqp.connect(CLOUDAMQP_URL, function(error0, connection) {
  if (error0) {
    throw error0;
  }
  connection.createChannel(function(error1, channel) {
    if (error1) {
      throw error1;
	}
	console.log('channel created.');
	var queue = 'meteo'; 
	pubChannel = channel; 
    channel.assertQueue(queue, {
      durable: false
    });
  });
});

function postMessage(msg){
	pubChannel.sendToQueue('meteo', Buffer.from(msg));
	console.log(" [x] Sent %s", msg);
}

/* /api/mesures */
app.post('/api/mesures', bodyParser, (req, res) => {

	var ok = 
		req.body.postion !== null && 
		req.body.position !== undefined &&
		req.body.position.latitude !== null && 
		req.body.position.latitude !== undefined && 
		req.body.position.latitude >= -90 && 
		req.body.position.latitude <= 90 && 
		req.body.position.longitude !== null && 
		req.body.position.longitude !== undefined && 
		req.body.position.longitude >= -180 && 
		req.body.position.longitude <= 180 && 
		req.body.position.longitude !== null && 
		req.body.position.longitude !== undefined && 
		req.body.position.longitude >= -180 && 
		req.body.position.longitude <= 180 && 
		req.body.position.elevation !== null && 
		req.body.position.elevation !== undefined && 
		req.body.position.elevation >= 0 && 
		req.body.position.elevation <= 4000 && 
		req.body.weather_info !== null && 
		req.body.weather_info !== undefined &&
		req.body.weather_info.datetime !== null && 
		req.body.weather_info.datetime !== undefined &&
		req.body.weather_info.temperature !== null && 
		req.body.weather_info.temperature !== undefined &&
		req.body.weather_info.temperature >= -30 && 
		req.body.weather_info.temperature <= 42 &&
		req.body.weather_info.wind_speed !== null && 
		req.body.weather_info.wind_speed !== undefined &&
		req.body.weather_info.wind_speed >= 0 && 
		req.body.weather_info.wind_speed <= 408 &&
		req.body.weather_info.wind_gust !== null && 
		req.body.weather_info.wind_gust !== undefined &&
		req.body.weather_info.wind_gust > req.body.weather_info.wind_speed &&
		req.body.weather_info.wind_dir !== null && 
		req.body.weather_info.wind_dir !== undefined &&
		req.body.weather_info.wind_dir >= 0 && 
		req.body.weather_info.wind_dir <= 360 &&
		req.body.weather_info.pressure !== null && 
		req.body.weather_info.pressure !== undefined &&
		req.body.weather_info.pressure >= 980 && 
		req.body.weather_info.pressure <= 1080 &&
		req.body.weather_info.humidity !== null && 
		req.body.weather_info.humidity !== undefined &&
		req.body.weather_info.humidity >= 0 && 
		req.body.weather_info.humidity <= 75;

	if(!ok) {
		res.status(400).json({ error: "Wrong data object." })
	}
	else {
		var data = {
			position: {
				latitude: req.body.position.latitude,
				longitude: req.body.position.longitude,
				elevation: req.body.position.elevation
			},
			weather_info : {
				datetime: req.body.weather_info.datetime,
			    temperature: req.body.weather_info.temperature,
			    wind_speed: req.body.weather_info.wind_speed,
			    wind_gust: req.body.weather_info.wind_gust,
			    wind_dir: req.body.weather_info.wind_dir,
			    pressure: req.body.weather_info.pressure,
			    humidity: req.body.weather_info.humidity
			}
		}

	    clientElastic.index({
	        index: 'mesures',
	        body: JSON.stringify(data)
	    })
		    .then(() => res.status(201).json({ message: 'Observation météo enregistrée.' }))
			.catch(error => res.status(400).json({ error }));
			postMessage(JSON.stringify(data));
	}
});

app.listen(PORT_NUMBER, () => {
    console.log("Server is up at port: " + PORT_NUMBER);
}); 
